import './index.css'
import ReactDOM from 'react-dom';
import React from 'react';

import App from './App.jsx'



ReactDOM.render(
    <div id="app">
        <App />
    </div>,
    document.getElementById('root')
    )