import './Card.css'
import React from 'react'

export default (props) => {
    const estilo = {
        backgroundColor: props.color || '#F00',
        borderColor: props.color || '#F00',
    }
    return (
        <div className="Card" style={estilo}> 
            <div className="Titulo">{props.titulo}</div>
            <div className="Conteudo">{props.children}</div>
        </div>
    )
}